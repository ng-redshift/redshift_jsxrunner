/**
 * RunScript.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adobe.ns.InDesign.soap;

public class RunScript  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5664242312083124849L;
	private com.adobe.ns.InDesign.soap.RunScriptParameters runScriptParameters;

    public RunScript() {
    }

    public RunScript(
           com.adobe.ns.InDesign.soap.RunScriptParameters runScriptParameters) {
           this.runScriptParameters = runScriptParameters;
    }


    /**
     * Gets the runScriptParameters value for this RunScript.
     * 
     * @return runScriptParameters
     */
    public com.adobe.ns.InDesign.soap.RunScriptParameters getRunScriptParameters() {
        return runScriptParameters;
    }


    /**
     * Sets the runScriptParameters value for this RunScript.
     * 
     * @param runScriptParameters
     */
    public void setRunScriptParameters(com.adobe.ns.InDesign.soap.RunScriptParameters runScriptParameters) {
        this.runScriptParameters = runScriptParameters;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RunScript)) return false;
        RunScript other = (RunScript) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.runScriptParameters==null && other.getRunScriptParameters()==null) || 
             (this.runScriptParameters!=null &&
              this.runScriptParameters.equals(other.getRunScriptParameters())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRunScriptParameters() != null) {
            _hashCode += getRunScriptParameters().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RunScript.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ns.adobe.com/InDesign/soap/", ">RunScript"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("runScriptParameters");
        elemField.setXmlName(new javax.xml.namespace.QName("", "runScriptParameters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ns.adobe.com/InDesign/soap/", "RunScriptParameters"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
