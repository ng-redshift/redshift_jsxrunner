/**
 * DataHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adobe.ns.InDesign.soap.holders;

public final class DataHolder implements javax.xml.rpc.holders.Holder {
    public com.adobe.ns.InDesign.soap.Data value;

    public DataHolder() {
    }

    public DataHolder(com.adobe.ns.InDesign.soap.Data value) {
        this.value = value;
    }

}
