/**
 * Object.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adobe.ns.InDesign.soap;

public class Object  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2386850172305930752L;

	private java.lang.Object specifierData;

    private org.apache.axis.types.UnsignedLong objectType;

    private org.apache.axis.types.UnsignedLong specifierForm;

    private com.adobe.ns.InDesign.soap.Object start;

    private com.adobe.ns.InDesign.soap.Object end;

    public Object() {
    }

    public Object(
           java.lang.Object specifierData,
           org.apache.axis.types.UnsignedLong objectType,
           org.apache.axis.types.UnsignedLong specifierForm,
           com.adobe.ns.InDesign.soap.Object start,
           com.adobe.ns.InDesign.soap.Object end) {
           this.specifierData = specifierData;
           this.objectType = objectType;
           this.specifierForm = specifierForm;
           this.start = start;
           this.end = end;
    }


    /**
     * Gets the specifierData value for this Object.
     * 
     * @return specifierData
     */
    public java.lang.Object getSpecifierData() {
        return specifierData;
    }


    /**
     * Sets the specifierData value for this Object.
     * 
     * @param specifierData
     */
    public void setSpecifierData(java.lang.Object specifierData) {
        this.specifierData = specifierData;
    }


    /**
     * Gets the objectType value for this Object.
     * 
     * @return objectType
     */
    public org.apache.axis.types.UnsignedLong getObjectType() {
        return objectType;
    }


    /**
     * Sets the objectType value for this Object.
     * 
     * @param objectType
     */
    public void setObjectType(org.apache.axis.types.UnsignedLong objectType) {
        this.objectType = objectType;
    }


    /**
     * Gets the specifierForm value for this Object.
     * 
     * @return specifierForm
     */
    public org.apache.axis.types.UnsignedLong getSpecifierForm() {
        return specifierForm;
    }


    /**
     * Sets the specifierForm value for this Object.
     * 
     * @param specifierForm
     */
    public void setSpecifierForm(org.apache.axis.types.UnsignedLong specifierForm) {
        this.specifierForm = specifierForm;
    }


    /**
     * Gets the start value for this Object.
     * 
     * @return start
     */
    public com.adobe.ns.InDesign.soap.Object getStart() {
        return start;
    }


    /**
     * Sets the start value for this Object.
     * 
     * @param start
     */
    public void setStart(com.adobe.ns.InDesign.soap.Object start) {
        this.start = start;
    }


    /**
     * Gets the end value for this Object.
     * 
     * @return end
     */
    public com.adobe.ns.InDesign.soap.Object getEnd() {
        return end;
    }


    /**
     * Sets the end value for this Object.
     * 
     * @param end
     */
    public void setEnd(com.adobe.ns.InDesign.soap.Object end) {
        this.end = end;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Object)) return false;
        Object other = (Object) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.specifierData==null && other.getSpecifierData()==null) || 
             (this.specifierData!=null &&
              this.specifierData.equals(other.getSpecifierData()))) &&
            ((this.objectType==null && other.getObjectType()==null) || 
             (this.objectType!=null &&
              this.objectType.equals(other.getObjectType()))) &&
            ((this.specifierForm==null && other.getSpecifierForm()==null) || 
             (this.specifierForm!=null &&
              this.specifierForm.equals(other.getSpecifierForm()))) &&
            ((this.start==null && other.getStart()==null) || 
             (this.start!=null &&
              this.start.equals(other.getStart()))) &&
            ((this.end==null && other.getEnd()==null) || 
             (this.end!=null &&
              this.end.equals(other.getEnd())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSpecifierData() != null) {
            _hashCode += getSpecifierData().hashCode();
        }
        if (getObjectType() != null) {
            _hashCode += getObjectType().hashCode();
        }
        if (getSpecifierForm() != null) {
            _hashCode += getSpecifierForm().hashCode();
        }
        if (getStart() != null) {
            _hashCode += getStart().hashCode();
        }
        if (getEnd() != null) {
            _hashCode += getEnd().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Object.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ns.adobe.com/InDesign/soap/", "Object"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specifierData");
        elemField.setXmlName(new javax.xml.namespace.QName("", "specifierData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("objectType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "objectType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specifierForm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "specifierForm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedLong"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("start");
        elemField.setXmlName(new javax.xml.namespace.QName("", "start"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ns.adobe.com/InDesign/soap/", "Object"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("end");
        elemField.setXmlName(new javax.xml.namespace.QName("", "end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ns.adobe.com/InDesign/soap/", "Object"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
