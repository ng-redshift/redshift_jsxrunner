/**
 * RunScriptResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adobe.ns.InDesign.soap;

public class RunScriptResponse  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1168938735954578323L;

	private int errorNumber;

    private java.lang.String errorString;

    private com.adobe.ns.InDesign.soap.Data scriptResult;

    public RunScriptResponse() {
    }

    public RunScriptResponse(
           int errorNumber,
           java.lang.String errorString,
           com.adobe.ns.InDesign.soap.Data scriptResult) {
           this.errorNumber = errorNumber;
           this.errorString = errorString;
           this.scriptResult = scriptResult;
    }


    /**
     * Gets the errorNumber value for this RunScriptResponse.
     * 
     * @return errorNumber
     */
    public int getErrorNumber() {
        return errorNumber;
    }


    /**
     * Sets the errorNumber value for this RunScriptResponse.
     * 
     * @param errorNumber
     */
    public void setErrorNumber(int errorNumber) {
        this.errorNumber = errorNumber;
    }


    /**
     * Gets the errorString value for this RunScriptResponse.
     * 
     * @return errorString
     */
    public java.lang.String getErrorString() {
        return errorString;
    }


    /**
     * Sets the errorString value for this RunScriptResponse.
     * 
     * @param errorString
     */
    public void setErrorString(java.lang.String errorString) {
        this.errorString = errorString;
    }


    /**
     * Gets the scriptResult value for this RunScriptResponse.
     * 
     * @return scriptResult
     */
    public com.adobe.ns.InDesign.soap.Data getScriptResult() {
        return scriptResult;
    }


    /**
     * Sets the scriptResult value for this RunScriptResponse.
     * 
     * @param scriptResult
     */
    public void setScriptResult(com.adobe.ns.InDesign.soap.Data scriptResult) {
        this.scriptResult = scriptResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RunScriptResponse)) return false;
        RunScriptResponse other = (RunScriptResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.errorNumber == other.getErrorNumber() &&
            ((this.errorString==null && other.getErrorString()==null) || 
             (this.errorString!=null &&
              this.errorString.equals(other.getErrorString()))) &&
            ((this.scriptResult==null && other.getScriptResult()==null) || 
             (this.scriptResult!=null &&
              this.scriptResult.equals(other.getScriptResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getErrorNumber();
        if (getErrorString() != null) {
            _hashCode += getErrorString().hashCode();
        }
        if (getScriptResult() != null) {
            _hashCode += getScriptResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RunScriptResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ns.adobe.com/InDesign/soap/", ">RunScriptResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "errorNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorString");
        elemField.setXmlName(new javax.xml.namespace.QName("", "errorString"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scriptResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "scriptResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ns.adobe.com/InDesign/soap/", "Data"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
