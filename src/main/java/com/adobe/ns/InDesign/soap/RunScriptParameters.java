/**
 * RunScriptParameters.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adobe.ns.InDesign.soap;

public class RunScriptParameters  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5060986070721047658L;
	private java.lang.String scriptText;
    private java.lang.String scriptLanguage="javascript";
    private java.lang.String scriptFile;

    private com.adobe.ns.InDesign.soap.IDSPScriptArg[] scriptArgs;

    public RunScriptParameters() {
    }

    public RunScriptParameters(
           java.lang.String scriptText,
           java.lang.String scriptLanguage,
           java.lang.String scriptFile,
           com.adobe.ns.InDesign.soap.IDSPScriptArg[] scriptArgs) {
           this.scriptText = scriptText;
           this.scriptLanguage = scriptLanguage;
           this.scriptFile = scriptFile;
           this.scriptArgs = scriptArgs;
    }


    /**
     * Gets the scriptText value for this RunScriptParameters.
     * 
     * @return scriptText
     */
    public java.lang.String getScriptText() {
        return scriptText;
    }


    /**
     * Sets the scriptText value for this RunScriptParameters.
     * 
     * @param scriptText
     */
    public void setScriptText(java.lang.String scriptText) {
        this.scriptText = scriptText;
    }


    /**
     * Gets the scriptLanguage value for this RunScriptParameters.
     * 
     * @return scriptLanguage
     */
    public java.lang.String getScriptLanguage() {
        return scriptLanguage;
    }


    /**
     * Sets the scriptLanguage value for this RunScriptParameters.
     * 
     * @param scriptLanguage
     */
    public void setScriptLanguage(java.lang.String scriptLanguage) {
        this.scriptLanguage = scriptLanguage;
    }


    /**
     * Gets the scriptFile value for this RunScriptParameters.
     * 
     * @return scriptFile
     */
    public java.lang.String getScriptFile() {
        return scriptFile;
    }


    /**
     * Sets the scriptFile value for this RunScriptParameters.
     * 
     * @param scriptFile
     */
    public void setScriptFile(java.lang.String scriptFile) {
        this.scriptFile = scriptFile;
    }


    /**
     * Gets the scriptArgs value for this RunScriptParameters.
     * 
     * @return scriptArgs
     */
    public com.adobe.ns.InDesign.soap.IDSPScriptArg[] getScriptArgs() {
        return scriptArgs;
    }


    /**
     * Sets the scriptArgs value for this RunScriptParameters.
     * 
     * @param scriptArgs
     */
    public void setScriptArgs(com.adobe.ns.InDesign.soap.IDSPScriptArg[] scriptArgs) {
        this.scriptArgs = scriptArgs;
    }

    public com.adobe.ns.InDesign.soap.IDSPScriptArg getScriptArgs(int i) {
        return this.scriptArgs[i];
    }

    public void setScriptArgs(int i, com.adobe.ns.InDesign.soap.IDSPScriptArg _value) {
        this.scriptArgs[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RunScriptParameters)) return false;
        RunScriptParameters other = (RunScriptParameters) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.scriptText==null && other.getScriptText()==null) || 
             (this.scriptText!=null &&
              this.scriptText.equals(other.getScriptText()))) &&
            ((this.scriptLanguage==null && other.getScriptLanguage()==null) || 
             (this.scriptLanguage!=null &&
              this.scriptLanguage.equals(other.getScriptLanguage()))) &&
            ((this.scriptFile==null && other.getScriptFile()==null) || 
             (this.scriptFile!=null &&
              this.scriptFile.equals(other.getScriptFile()))) &&
            ((this.scriptArgs==null && other.getScriptArgs()==null) || 
             (this.scriptArgs!=null &&
              java.util.Arrays.equals(this.scriptArgs, other.getScriptArgs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getScriptText() != null) {
            _hashCode += getScriptText().hashCode();
        }
        if (getScriptLanguage() != null) {
            _hashCode += getScriptLanguage().hashCode();
        }
        if (getScriptFile() != null) {
            _hashCode += getScriptFile().hashCode();
        }
        if (getScriptArgs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getScriptArgs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getScriptArgs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RunScriptParameters.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ns.adobe.com/InDesign/soap/", "RunScriptParameters"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scriptText");
        elemField.setXmlName(new javax.xml.namespace.QName("", "scriptText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scriptLanguage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "scriptLanguage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scriptFile");
        elemField.setXmlName(new javax.xml.namespace.QName("", "scriptFile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scriptArgs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "scriptArgs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ns.adobe.com/InDesign/soap/", "IDSP-ScriptArg"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
