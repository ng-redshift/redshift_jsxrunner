//========================================================================================
//  
//  $File: //depot/devtech/ids/cs5.5/samples/sampleclient-java-soap/src/com/adobe/ids/CommandLineArguments.java $
//  
//  Owner: Kirk Mattson
//  
//  $Author: weiguo $
//  
//  $DateTime: 2010/06/07 13:46:48 $
//  
//  $Revision: #1 $
//  
//  $Change: 753115 $
//  
// ADOBE SYSTEMS INCORPORATED
//   Copyright 2010 Adobe Systems Incorporated
//   All Rights Reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance with the 
//  terms of the Adobe license agreement accompanying it.  If you have received this file from a 
//  source other than Adobe, then your use, modification, or distribution of it requires the prior 
//  written permission of Adobe. 
//  
//========================================================================================

package com.adobe.ids;

import java.util.HashSet;
import java.util.Set;

import com.adobe.ns.InDesign.soap.IDSPScriptArg;
import com.mongodb.BasicDBObject;

public class CommandLineArguments
{	
	String host = new String();
	String scriptPath = new String();
	int repeat = 1;
	boolean specifyScriptFile = false;
	public Set scriptArgs = new HashSet();
	
	public CommandLineArguments(BasicDBObject jsonObject)
	{
		host=jsonObject.getString("host");
		scriptPath=jsonObject.getString("script");
		jsonObject.remove("host");
		jsonObject.remove("script");
		
		for(String key : jsonObject.keySet() ){
			scriptArgs.add(new IDSPScriptArg(key,jsonObject.getString(key)));
			}
		
		// The host and scriptpath parameter are required
		// so complain if they weren't specified.
		if(host.length() == 0 || scriptPath.length() == 0){
			throw new IllegalArgumentException(new String());
		}
	}

	public String getScriptPath()
	{
		return scriptPath;
	}

	public void setScriptPath(String scriptPath)
	{
		this.scriptPath = scriptPath;
	}
	
	public String getHost()
	{
		return host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

	public int getRepeat()
	{
		return repeat;
	}

	public void setRepeat(int repeat)
	{
		this.repeat = repeat;
	}

	public boolean isSpecifyScriptFile()
	{
		return specifyScriptFile;
	}

	public void setSpecifyScriptFile(boolean specifyScriptFile)
	{
		this.specifyScriptFile = specifyScriptFile;
	}
}
