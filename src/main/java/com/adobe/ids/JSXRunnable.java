package com.adobe.ids;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;

import javax.xml.rpc.holders.IntHolder;
import javax.xml.rpc.holders.StringHolder;

import org.apache.commons.io.IOUtils;

import localhost.Service_wsdl.Service;
import localhost.Service_wsdl.ServiceLocator;
import localhost.Service_wsdl.ServicePortType;

import com.adobe.ns.InDesign.soap.IDSPScriptArg;
import com.adobe.ns.InDesign.soap.RunScriptParameters;
import com.adobe.ns.InDesign.soap.holders.DataHolder;
import com.mongodb.BasicDBObject;

public class JSXRunnable{
    private static Integer port = null;
    static BasicDBObject dbObject=new BasicDBObject();
  	
    
    
    public static void main(String[] args) throws Exception { 
		try{			
			port=Integer.parseInt(args[0]);
			String inputstring = "";
			FileInputStream inputStream=null;
			String scriptFile=args[1];
			try{
				
				if (!new File(scriptFile).isFile()) {
					System.out.println("\""+scriptFile+"\" is not exists...!");
					return;
				}
				File jsonFile = new File(args[2]);
				if (jsonFile.isFile() && jsonFile.canRead()) {
					inputStream = new FileInputStream(jsonFile);
					inputstring = IOUtils.toString(inputStream);
				}
				
			}catch(Exception e){
		        try{
					inputStream.close();
				}catch(IOException e1){
					e1.printStackTrace();
				}
		    }
			BasicDBObject dbObject=BasicDBObject.parse(inputstring.replace(": null,",": \"\","));	
			//System.out.println(dbObject.toString());
			CommandLineArguments parsedArgs=null;
			String myURL="http://127.0.0.1:"+port+"/service?wsdl";
			dbObject.append("host", "http://127.0.0.1:"+port);
			dbObject.append("script", scriptFile);
			dbObject.append("json", args[2]);
			try {
				parsedArgs=new CommandLineArguments(dbObject);
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}
			RunScriptParameters runScriptParams=new RunScriptParameters();
			runScriptParams.setScriptArgs((IDSPScriptArg[])parsedArgs.scriptArgs.toArray(new IDSPScriptArg[parsedArgs.scriptArgs.size()]));
			runScriptParams.setScriptFile(parsedArgs.getScriptPath());
			Service inDesignService=new ServiceLocator();
			ServicePortType inDesignServer=inDesignService.getService(new URL(parsedArgs.getHost()));
			IntHolder errorNumber=new IntHolder(0);
			StringHolder errorString = new StringHolder();
			DataHolder results = new DataHolder();	
			try {
			inDesignServer.runScript(runScriptParams,errorNumber,errorString,results);
			}catch (RemoteException e) {
				//System.err.println("REDSHIFT: The Remote exception:-->"+e.getMessage()+", Line: "+e.getStackTrace()[0].getLineNumber()+",\n\n"+e.detail);
				// TODO: handle exception
				try{
					URL url=new URL(myURL);
					URLConnection urlConn=url.openConnection();
					int i=0;
					System.out.println("Heartbeat check after timeout...!");
					while(((urlConn==null)||(urlConn.getInputStream()==null)) && i<300){
						System.out.println("check "+i);
						Thread.sleep(5000);
						i++;
					}
				}catch(java.net.SocketTimeoutException ex) {
					System.out.println("No heart beat");
				}
			}
			System.out.println("Following port run successfully and added: "+port);
			dbObject.clear();
		}catch(IllegalArgumentException e){
			System.err.println("REDSHIFT: Illegal arguments:"+e.getMessage()+e.getStackTrace()[0].getLineNumber());
		}catch(IOException e){		
			System.err.println("REDSHIFT: The IO exception:-->"+e.getMessage()+", Line: "+e.getStackTrace()[0].getLineNumber());
		}catch(Exception e){
			System.err.println("REDSHIFT: EX"+e.getMessage()+e.getStackTrace()[0].getLineNumber());
		}
    }//main
}