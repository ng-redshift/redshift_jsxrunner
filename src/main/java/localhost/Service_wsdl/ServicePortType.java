/**
 * ServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package localhost.Service_wsdl;

public interface ServicePortType extends java.rmi.Remote {

    /**
     * Service definition of function IDSP__RunScript
     */
    public void runScript(com.adobe.ns.InDesign.soap.RunScriptParameters runScriptParameters, javax.xml.rpc.holders.IntHolder errorNumber, javax.xml.rpc.holders.StringHolder errorString, com.adobe.ns.InDesign.soap.holders.DataHolder scriptResult) throws java.rmi.RemoteException;
}
