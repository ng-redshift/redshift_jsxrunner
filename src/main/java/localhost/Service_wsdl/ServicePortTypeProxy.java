package localhost.Service_wsdl;

import java.rmi.RemoteException;

import javax.xml.rpc.holders.IntHolder;
import javax.xml.rpc.holders.StringHolder;

import com.adobe.ns.InDesign.soap.RunScriptParameters;
import com.adobe.ns.InDesign.soap.holders.DataHolder;

public class ServicePortTypeProxy implements localhost.Service_wsdl.ServicePortType {
  private String _endpoint = null;
  private localhost.Service_wsdl.ServicePortType servicePortType = null;
  
  public ServicePortTypeProxy() {
    _initServicePortTypeProxy();
  }
  
  public ServicePortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initServicePortTypeProxy();
  }
  
  private void _initServicePortTypeProxy() {
    try {
      servicePortType = (new localhost.Service_wsdl.ServiceLocator()).getService();
      if (servicePortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)servicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)servicePortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (servicePortType != null)
      ((javax.xml.rpc.Stub)servicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public localhost.Service_wsdl.ServicePortType getServicePortType() {
    if (servicePortType == null)
      _initServicePortTypeProxy();
    return servicePortType;
  }

  public void runScript(RunScriptParameters runScriptParameters,
		IntHolder errorNumber, StringHolder errorString, DataHolder scriptResult)
		throws RemoteException {
	  if (servicePortType == null)
	      _initServicePortTypeProxy();
	    servicePortType.runScript(runScriptParameters, errorNumber, errorString, scriptResult);
  }  
}